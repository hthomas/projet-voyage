<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    public function index(Request $request) {
        return $this->render('admin.html.twig', []);
    }
    /**
     *@Route("/voyage/admin")
    */
    public function direct($valeur = 'inconnu')
    {
        return $this->redirect($this->generateUrl('visiteur', array('name'=> $valeur)));
    } 
}?>