<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Avis;
use App\Entity\Etablissement;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class EtablissementController extends AbstractController
{
    /**
     * @Route("/ajoutEtablissement", name="ajout_etablissement")
     */
    public function ajoutEtablissement(Request $request)
    {
        $etablissement = new Etablissement();
        $etablissement->setNom('Seven Hotel');
        $etablissement->setAdresse('20 Rue Berthollet, 75005 Paris');
        $etablissement->setTel('01 43 31 47 52');
        $etablissement->setEmail('contact@sevenhotelparis.com');
        $avis = $this->getDoctrine()->getRepository(Avis::class)->find(1);
        $etablissement->addAvi($avis);

        //Récupération de l’EntityManager
        $em = $this->getDoctrine()->getManager();
        //gestion de $etablissement par l’ORM
        $em->persist($etablissement);
        //l’ORM regarde les objets qu’il gère pour savoir s’ils doivent être persistés
        $em->flush();

        return $this->render('etablissement/ajout.html.twig',[
            'avis'=>$avis,
            'etablissement'=>$etablissement,
            ]);
    }
}