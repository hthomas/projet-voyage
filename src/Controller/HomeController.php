<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController
{
    public function index(Request $request) {
        return $this->render('base.html.twig', ['name' => $request->get('nom', 'inconnu'),'pays' => $request->get('pays', 'inconnu')]);
    }
}?>