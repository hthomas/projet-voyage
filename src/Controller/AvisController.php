<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Avis;
use App\Entity\Membre;
use App\Entity\Etablissement;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\{TextType,ButtonType,EmailType,HiddenType,PasswordType,TextareaType,SubmitType,NumberType,DateType,MoneyType,BirthdayType};
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AvisController extends AbstractController
{
    /**
     * @Route("/ajoutAvis", name="ajout_avis")
     */
    public function ajoutAvis(Request $request)
    {
        $avis = new Avis();
        $avis->setTitre('Titre de l\'avis');
        $avis->setNote(5);
        $avis->setDate(new \DateTime());
        $avis->setCommentaire('Commentaire de l\'avis');
        $membre = $this->getDoctrine()->getRepository(Membre::class)->find(1);
        $avis->setLeMembre($membre);

        //Récupération de l’EntityManager
        $em = $this->getDoctrine()->getManager();
        //gestion de $client par l’ORM
        $em->persist($avis);
        //l’ORM regarde les objets qu’il gère pour savoir s’ils doivent être persistés
        $em->flush();

        return $this->render('avis/ajout.html.twig',[
            'avis'=>$avis,
            'membre'=>$membre,
            ]);
    }

    /**
     * @Route("/ajouterAvis", name="ajouter_avis")
     */
    public function ajouter_avis(Request $request)
    {
        //on crée un Avis
        $unAvis = new Avis();

        //on créer un formulaire via formbuilder
        $form = $this->createFormBuilder($unAvis)
            ->add('le_membre', EntityType::class, [
                'class' => Membre::class,
                'choice_label' => 'Nom',
                'label' => 'Membre '])
            ->add('titre', TextType::class, ['label' => 'Titre avis '])
            ->add('note', TextType::class, ['label' => 'Note avis '])
            ->add('date', DateType::class, ['label' => 'Date avis '])
            ->add('commentaire', TextareaType::class, ['label' => 'Commentaire avis '])
            ->add('etablissement', EntityType::class, [
                'class' => Etablissement::class,
                'choice_label' => 'Nom',
                'label' => 'Etablissement '])
            ->add('envoyer', SubmitType::class)
            ->getForm();

        //Si on clique sur Envoyer on récup les données et on enregistre l'avis
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            //Récupération des données
            $unAvis = $form->getData();

            //Récupération de l’EntityManager
            $em = $this->getDoctrine()->getManager();

            //gestion de $avis par l’ORM
            $em->persist($unAvis);

            //l’ORM regarde les objets qu’il gère pour savoir s’ils doivent être persistés
            $em->flush();

            return new Response("L'Avis a été enregistré");
        }

        return $this->render('avis/ajouterAvis.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/afficherAvis", name="afficher_avis")
     */
    public function afficherAvis(Request $request)
    {
        $avis = $this->getDoctrine()->getRepository(Avis::class)->find(2);

        return $this->render('avis/afficher.html.twig',[
            'avis'=>$avis,
            ]);
    }
}