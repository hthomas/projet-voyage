<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Etablissement;
use App\Entity\Equipement;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class EquipementController extends AbstractController
{
    /**
     * @Route("/etablissement/ajoutEquipement/{libelle}", name="ajout_equipement")
     */
    public function ajoutEquipement($libelle)
    {
        $equipement = new Equipement();
        $equipement->setLibelle($libelle);
        $etablissement = $this->getDoctrine()->getRepository(Etablissement::class)->find(2);
        $equipement->addEtablissement($etablissement);

        //Récupération de l’EntityManager
        $em = $this->getDoctrine()->getManager();
        //gestion de $client par l’ORM
        $em->persist($equipement);
        //l’ORM regarde les objets qu’il gère pour savoir s’ils doivent être persistés
        $em->flush();

        return $this->render('equipement/ajout.html.twig',[
            'equipement'=>$equipement,
            'etablissement'=>$etablissement,
            ]);
    }

    /**
     * @Route("/equipement/ajoutEtablissement/{id}", name="ajout_equip_etablissement")
     */
    public function ajoutEtablissement($id)
    {
        $etablissement = $this->getDoctrine()->getRepository(Etablissement::class)->find($id);
        //Sauna = ID 2 
        $equipement = $this->getDoctrine()->getRepository(Equipement::class)->find(2);
        $etablissement->addEquipement($equipement);
        $equip = $equipement->getEtablissements();

        //Récupération de l’EntityManager
        $em = $this->getDoctrine()->getManager();
        //gestion de $client par l’ORM
        $em->persist($etablissement);
        //l’ORM regarde les objets qu’il gère pour savoir s’ils doivent être persistés
        //$em->flush();

        return $this->render('equipement/ajout_etablissement.html.twig',[
            'equipement'=>$equipement,
            'equip'=>$equip,
            ]);
    }
}