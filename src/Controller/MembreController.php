<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Membre;
use Symfony\Component\Form\Extension\Core\Type\{TextType,ButtonType,EmailType,HiddenType,PasswordType,TextareaType,SubmitType,NumberType,DateType,MoneyType,BirthdayType};


class MembreController extends AbstractController
{
    /**
     * @Route("/membre", name="membre")
     */
    public function index()
    {
        return $this->render('membre/index.html.twig', [
            'controller_name' => 'MembreController',
        ]);
    }
    public function ajout(Request $request)
    {
        $nom = $request->get('nom', 'inconnu');
        $prenom = $request->get('prenom', 'inconnu');
        $email = $request->get('email', 'inconnu');

        // Création de l'entité
        $membre = new Membre();
        $membre->setNom($nom);
        $membre->setPrenom($prenom);
        $membre->setEmail($email);

        //Récupération de l’EntityManager
        $em = $this->getDoctrine()->getManager();

        //gestion de $membre par l’ORM
        $em->persist($membre);

        //l’ORM regarde les objets qu’il gère pour savoir s’ils doivent être persistés
        $em->flush(); 

        return $this->render('membre/ajout.html.twig', ['nom' => $nom,'prenom' => $prenom,'id' => $membre->getId(),]);
    }
    /**
     * @Route("/afficher/membre/{id}", name="afficher_membre")
     */
    public function afficher($id)
    {
        $membre = $this->getDoctrine()->getRepository(membre::class)->find($id);

        if (!$membre) {
            throw $this->createNotFoundException('pas de membre trouvé pour identifiant '.$id);
        } else {        
            $id = $membre->getId();
            $nom = $membre->getNom();
            $prenom = $membre->getPrenom();
            $email = $membre->getEmail();

            return $this->render('membre/ajout.html.twig', ['nom' => $nom,'prenom' => $prenom,'id' => $id,'email' => $email,]);
        }
    }
    /**
     * @Route("/ajouterMembre", name="ajouter_membres")
     */
    public function ajouter_membres(Request $request)
    {
        //on crée un membre
        $unMembre = new Membre();

        //on créer un formulaire via formbuilder
        $form = $this->createFormBuilder($unMembre)
            ->add('nom', TextType::class, ['label' => 'Nom membre '])
            ->add('prenom', TextType::class, ['label' => 'Prenom membre '])
            ->add('email', TextType::class, ['label' => 'Email membre '])
            ->add('envoyer', SubmitType::class)
            ->getForm();

        //Si on clique sur Envoyer on récup les données et on enregistre le membre
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            //Récupération des données
            $unMembre = $form->getData();
            
            //Récupération de l’EntityManager
            $em = $this->getDoctrine()->getManager();

            //gestion de $membre par l’ORM
            $em->persist($unMembre);

            //l’ORM regarde les objets qu’il gère pour savoir s’ils doivent être persistés
            $em->flush();

            return new Response("Le Membre a été enregistré");
        }

        return $this->render('membre/ajouterMembre.html.twig', array('form' => $form->createView()));
    }
}
